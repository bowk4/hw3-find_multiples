let userInput;
let isValid = false;

while (!isValid) {
    userInput = prompt("Enter a number:");

    // Перевірка, чи є введене значення цілим числом
    if (Number.isInteger(Number(userInput))) {
        isValid = true;
    } else {
        alert("Incorrect expression entered!");
    }
}

if (userInput !== null && !isNaN(userInput)) {
    let number = parseInt(userInput);
    let divisibleByFive = false;

    for (let i = 1; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);
            divisibleByFive = true;
        }
    }
    if (!divisibleByFive) {
        console.log("Sorry, no numbers");
    }
} else {
    console.log("Incorrect expression entered!");
}


