function isPrime(number) {
    if (number <= 1) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(number); i++) {
        if (number % i === 0) {
            return false;
        }
    }
    return true;
}

function getPrimesInRange() {
    let m, n;

    while (true) {
        m = parseInt(prompt('Введіть перше число (m):'));
        n = parseInt(prompt('Введіть друге число (n):'));

        if (isNaN(m) || isNaN(n)) {
            console.log('Помилка: Введені значення повинні бути числами. Будь ласка, спробуйте ще раз.');
            continue;
        }

        if (m >= n) {
            console.log('Помилка: Перше число (m) повинно бути меншим за друге число (n). Будь ласка, спробуйте ще раз.');
            continue;
        }

        break;
    }

    console.log(`Прості числа в діапазоні від ${m} до ${n}:`);
    for (let i = m; i <= n; i++) {
        if (isPrime(i)) {
            console.log(i);
        }
    }
}

getPrimesInRange();